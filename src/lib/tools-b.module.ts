import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Test1Component } from './test1/test1.component';
import { ToolsBRoutingModule } from './tools-b-routing.module';

@NgModule({
  imports: [CommonModule, ToolsBRoutingModule],
  declarations: [Test1Component],
})
export class ToolsBModule {}
